import org.apache.http.HttpHost;
import org.elasticsearch.action.admin.indices.get.GetIndexRequest;
import org.elasticsearch.action.search.*;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class GetIndecies {


    public static String[] getAllIndecies() throws UnknownHostException {
    /*

      This is absolutely unneccessary because i dont understand what i read,
      this return the indices (indexes with wrong english) in a String array

     */
        Client client = new PreBuiltTransportClient(Settings.EMPTY)
                .addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300));



        return client.admin()
                .indices()
                .getIndex(new GetIndexRequest())
                .actionGet()
                .getIndices();

    }

    public static String getAllData() throws UnknownHostException {
        final Scroll scroll = new Scroll(TimeValue.timeValueMinutes(1L));
        String out="" ;
        RestHighLevelClient client = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("localhost", 9200, "http")));


        SearchRequest searchRequest = new SearchRequest("structured_output_small");
        searchRequest.scroll(scroll);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        searchSourceBuilder.fetchSource("text", null);
        searchRequest.source(searchSourceBuilder);


        SearchResponse searchResponse = null;
        try {
            searchResponse = client.search(searchRequest);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String scrollId = searchResponse.getScrollId();
        SearchHit[] searchHits = searchResponse.getHits().getHits();

        while (searchHits != null && searchHits.length > 0) {
            SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
            scrollRequest.scroll(scroll);
            try {
                searchResponse = client.searchScroll(scrollRequest);
            } catch (IOException e) {
                e.printStackTrace();
            }
            scrollId = searchResponse.getScrollId();
            searchHits = searchResponse.getHits().getHits();

            for(SearchHit hit : searchHits){

                String sourceAsString = hit.getSourceAsString();
                String[] split=hit.getSourceAsString().split("\"");
            if (out.isEmpty())
                out=split[3]+" ";

            else
                out+=split[3]+" ";
            }
        }


        ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
        clearScrollRequest.addScrollId(scrollId);
        ClearScrollResponse clearScrollResponse = null;
        try {
            clearScrollResponse = client.clearScroll(clearScrollRequest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean succeeded = clearScrollResponse.isSucceeded();

        if(succeeded)
            System.out.println("succeeded getting data");

        out=out.replace(","," ");
        out=out.replace("\n"," ");
        out=out.replace("..."," ");
        out=out.replace("."," ");
        out=out.replace("'"," ");
        out=out.replace("{"," ");
        out=out.replace("}"," ");
        out=out.replace("["," ");
        out=out.replace("]"," ");

        out=out.replace(":"," ");

        return out;
    }


}
