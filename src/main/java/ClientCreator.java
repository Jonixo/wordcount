import org.apache.http.HttpHost;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import scala.Tuple2;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class ClientCreator {

    private static  RestHighLevelClient client;
    private static Map<String,Object> jsonMap=new HashMap<>();
    static{
        client= new RestHighLevelClient(
                    RestClient.builder(
                            new HttpHost("localhost", 9200, "http")));
        }

        public static void addToMap(String key, String value){
        if(!key.isEmpty() )
                jsonMap.put(key,value);

        }


        public static void index(String indexName){

            IndexRequest indexRequest = new IndexRequest(indexName, "doc", "1")
                    .source(jsonMap);
            try {
                client.index(indexRequest);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }


        public static int CustomComaprator(Object o11, Object o12)  {
            Tuple2<Integer, String> o1 = (Tuple2<Integer, String>) o11;
            Tuple2<Integer, String> o2 = (Tuple2<Integer, String>) o12;
            if (o1._1 > o2._1)
                 return o1._1;
            else
                return o2._1;
        }

}
