import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

public class SparkMapReduce {

    public static void main(String[] args) {
        java.util.Date date = new java.util.Date();
        SparkConf conf = new
                SparkConf().setMaster("local[2]").setAppName("MapReduce");
        JavaSparkContext sc = new JavaSparkContext(conf);

        List<String> wordList=null;
        try {
             wordList = Arrays.asList(GetIndecies.getAllData());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }


        JavaRDD<String> rdd = sc.parallelize(wordList);

        JavaPairRDD<String, Integer> counts = rdd.flatMap(x -> Arrays.asList(x.split(" ")).iterator())
                .mapToPair(x -> new Tuple2<>(x, 1))
                .reduceByKey((x, y) -> x + y);

        JavaPairRDD<Integer, String> swappedPair = counts.mapToPair(new PairFunction<Tuple2<String, Integer>, Integer, String>() {
            @Override
            public Tuple2<Integer, String> call(Tuple2<String, Integer> item) throws Exception {
                return item.swap();
            }

        });



        ClientCreator.addToMap("date",date.toString());
        ClientCreator.addToMap("Total Count",counts.count()+"");

       swappedPair.takeOrdered(490,new TupleComparator()).forEach(record->{
           ClientCreator.addToMap(record._2,record._1+"");
           System.out.println(record);
       });

        ClientCreator.index("twitter_statistics");

    }
}


